package org.byters.libnetworkdata.controller.data;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;

public class CacheStorage implements ICacheStorage {

    @Override
    public void writeToLogServiceState(int state) {
        writeData("service state " + state);
    }

    private void writeData(String data) {
        try {
            FileOutputStream fOut = new FileOutputStream(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + File.separator + "gsmDataLog.txt",
                    true);

            OutputStreamWriter osw = new OutputStreamWriter(fOut);
            osw.write(String.format("%d: %s%s", System.currentTimeMillis(), data,"\n"));
            osw.flush();
            osw.close();
        } catch (IOException e) {
        }
    }

    @Override
    public void writeToLogGsmSignalStength(int gsmSignalStrength) {
        writeData(String.format("gsmSignalStrength %d", gsmSignalStrength));
    }

    @Override
    public void writeToLogGsmCellLocation(int cid, int lac, int psc) {
        writeData(String.format("gsmCellLocation cid: %d, lac: %d, psc:%d", cid, lac, psc));
    }
}
