package org.byters.libnetworkdata.controller.data;

public interface ICacheStorage {
    void writeToLogServiceState(int state);

    void writeToLogGsmSignalStength(int gsmSignalStrength);

    void writeToLogGsmCellLocation(int cid, int lac, int psc);
}
