package org.byters.libnetworkdata.controller;

import android.content.ServiceConnection;

import org.byters.libnetworkdata.ILibCallback;
import org.byters.libnetworkdata.controller.data.CacheStorage;
import org.byters.libnetworkdata.controller.data.ICacheStorage;
import org.byters.libnetworkdata.service.GSMServiceBinder;
import org.byters.libnetworkdata.service.GSMServiceConnection;
import org.byters.libnetworkdata.service.StateListener;

import java.lang.ref.WeakReference;

public class Injector {

    private static WeakReference<Injector> refInstance;
    private GSMServiceConnection gsmServiceConnection;
    private WeakReference<ILibCallback> refServiceBinderCallback;
    private GSMServiceBinder serviceBinder;
    private StateListener stateListener;
    private ICacheStorage cacheStorage;

    public Injector() {
        refInstance = new WeakReference<>(this);
    }

    public static Injector getInstance() {
        return refInstance == null ? null : refInstance.get();
    }

    public ServiceConnection getServiceConnection() {
        if (gsmServiceConnection == null) gsmServiceConnection = new GSMServiceConnection();
        return gsmServiceConnection;
    }

    public void setLibCallback(ILibCallback callback) {
        this.refServiceBinderCallback = new WeakReference<>(callback);
        getStateListener().setCallback(refServiceBinderCallback);
    }

    public GSMServiceBinder getServiceBinder() {
        if (serviceBinder == null) serviceBinder = new GSMServiceBinder();
        return serviceBinder;
    }

    public StateListener getStateListener() {
        if (stateListener == null) stateListener = new StateListener(getCacheStorage());
        return stateListener;
    }

    private ICacheStorage getCacheStorage() {
        if (cacheStorage == null) cacheStorage = new CacheStorage();
        return cacheStorage;
    }
}
