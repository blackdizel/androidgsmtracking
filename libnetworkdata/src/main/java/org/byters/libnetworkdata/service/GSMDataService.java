package org.byters.libnetworkdata.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import org.byters.libnetworkdata.controller.Injector;

public class GSMDataService extends Service {

    private StateListener stateListener;

    @Override
    public IBinder onBind(Intent intent) {
        subscribeDataChange();

        return Injector.getInstance() == null
                ? null
                : Injector.getInstance().getServiceBinder();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        subscribeDataChange();
        return Service.START_STICKY;
    }

    private void subscribeDataChange() {

        if (Injector.getInstance() == null) return;

        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        manager.listen(stateListener = Injector.getInstance().getStateListener(),
                PhoneStateListener.LISTEN_CELL_LOCATION
                        | PhoneStateListener.LISTEN_SIGNAL_STRENGTHS
                        | PhoneStateListener.LISTEN_SERVICE_STATE);
    }

}
