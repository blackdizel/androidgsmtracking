package org.byters.libnetworkdata.service;

import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.gsm.GsmCellLocation;

import org.byters.libnetworkdata.ILibCallback;
import org.byters.libnetworkdata.controller.data.ICacheStorage;

import java.lang.ref.WeakReference;

public class StateListener extends PhoneStateListener {

    private WeakReference<ICacheStorage> refCacheStorage;
    private WeakReference<ILibCallback> refCallback;

    public StateListener(ICacheStorage cacheStorage) {
        this.refCacheStorage = new WeakReference<>(cacheStorage);
    }

    @Override
    public void onCellLocationChanged(CellLocation location) {
        if (location == null || !(location instanceof GsmCellLocation))
            return; //get data only for GSM
        notifyListeners((GsmCellLocation) location);
    }

    @Override
    public void onSignalStrengthsChanged(SignalStrength signalStrength) {
        if (signalStrength == null) return;
        notifyListeners(signalStrength);
    }

    @Override
    public void onServiceStateChanged(ServiceState serviceState) {
        if (serviceState == null) return;
        notifyListeners(serviceState);
    }

    private void sendServiceState(int state) {
        if (refCallback == null || refCallback.get() == null) return;
        refCallback.get().updateServiceState(state);
    }

    private void sendSignalStrength(int gsmSignalStrength) {
        if (refCallback == null || refCallback.get() == null) return;
        refCallback.get().updateSignalStrength(gsmSignalStrength);
    }

    private void sendGsmCellLocation(int cid, int lac, int psc) {
        if (refCallback == null || refCallback.get() == null) return;
        refCallback.get().updateGsmCellLocation(cid, lac, psc);
    }

    public void setCallback(WeakReference<ILibCallback> refCallback) {
        this.refCallback = refCallback;
    }

    private void notifyListeners(ServiceState serviceState) {
        if (refCacheStorage != null && refCacheStorage.get() != null)
            refCacheStorage.get().writeToLogServiceState(serviceState.getState());
        sendServiceState(serviceState.getState());
    }

    private void notifyListeners(SignalStrength signalStrength) {
        if (refCacheStorage != null && refCacheStorage.get() != null)
            refCacheStorage.get().writeToLogGsmSignalStength(signalStrength.getGsmSignalStrength());
        sendSignalStrength(signalStrength.getGsmSignalStrength());
    }

    private void notifyListeners(GsmCellLocation location) {
        if (refCacheStorage != null && refCacheStorage.get() != null)
            refCacheStorage.get().writeToLogGsmCellLocation(location.getCid(), location.getLac(), location.getPsc());
        sendGsmCellLocation(location.getCid(), location.getLac(), location.getPsc());
    }
}
