package org.byters.libnetworkdata;

public interface ILibCallback {
    void updateServiceState(int state);

    void updateSignalStrength(int gsmSignalStrength);

    void updateGsmCellLocation(int cid, int lac, int psc);
}
