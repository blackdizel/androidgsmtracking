package org.byters.libnetworkdata;

import android.content.Context;
import android.content.Intent;

import org.byters.libnetworkdata.controller.Injector;
import org.byters.libnetworkdata.service.GSMDataService;

public class LibNetworkData {

    private Injector injector;

    public LibNetworkData() {
        injector = new Injector();
    }

    public void setCallback(ILibCallback callback) {
        injector.setLibCallback(callback);
    }

    public void onStart(Context context) {
        Intent intent = new Intent(context, GSMDataService.class);
        context.startService(intent);
        //context.bindService(intent, injector.getServiceConnection(), Context.BIND_AUTO_CREATE);
    }

    public void onStop(Context context) {
        // context.unbindService(injector.getServiceConnection());
    }

}
