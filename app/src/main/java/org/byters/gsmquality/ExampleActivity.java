package org.byters.gsmquality;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.ServiceState;
import android.widget.TextView;

import org.byters.libnetworkdata.ILibCallback;

public class ExampleActivity extends AppCompatActivity {

    private static final int CODE_PERMISSIONS = 123;
    private LibCallback libCallback;
    private TextView tvServiceState, tvGsmSignalStrength, tvGsmCellLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvServiceState = findViewById(R.id.tvServiceState);
        tvGsmSignalStrength = findViewById(R.id.tvGsmSignalStrength);
        tvGsmCellLocation = findViewById(R.id.tvGsmCellLocation);

        ((ExampleApplication) getApplicationContext()).getLibNetworkData().setCallback(libCallback = new LibCallback());


        checkPermissions();

    }

    private void checkPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    CODE_PERMISSIONS);
        }
    }

    private boolean isPermissionGranted(int requestCode, int[] grantResults) {
        return requestCode == CODE_PERMISSIONS
                && grantResults != null
                && grantResults.length == 3
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED
                && grantResults[2] == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (!isPermissionGranted(requestCode, grantResults)) return;
        ((ExampleApplication) getApplicationContext()).getLibNetworkData().onStart(this);
    }

    private class LibCallback implements ILibCallback {
        @Override
        public void updateServiceState(int state) {
            tvServiceState.setText(String.format("service state %s(%d)", getServiceStateString(state), state));
        }

        private String getServiceStateString(int state) {
            if (state == ServiceState.STATE_IN_SERVICE)
                return "in service";
            if (state == ServiceState.STATE_EMERGENCY_ONLY)
                return "emergency only";
            if (state == ServiceState.STATE_OUT_OF_SERVICE)
                return "out of service";
            if (state == ServiceState.STATE_POWER_OFF)
                return "power off";
            return "unknown";
        }

        @Override
        public void updateSignalStrength(int gsmSignalStrength) {
            tvGsmSignalStrength.setText(String.format("gsm signal strength %s", String.valueOf(gsmSignalStrength)));
        }

        @Override
        public void updateGsmCellLocation(int cid, int lac, int psc) {
            tvGsmCellLocation.setText(String.format("GSM cell location cid: %d, lac: %d, psc: %d", cid, lac, psc));
        }
    }
}
