package org.byters.gsmquality;

import android.app.Application;

import org.byters.libnetworkdata.LibNetworkData;

public class ExampleApplication extends Application {
    private LibNetworkData libNetworkData;

    @Override
    public void onCreate() {
        super.onCreate();
        libNetworkData = new LibNetworkData();
    }

    public LibNetworkData getLibNetworkData() {
        return libNetworkData;
    }
}
